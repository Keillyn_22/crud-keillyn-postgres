from Model import *

class Controller():
    model = Model()

    def menu(self):

        print("***Bienvenido(a)***")
        print("1.-Crear registros")
        print("2.-Ver registros")
        print("3.-Eliminar registro")
        print("4.-Modificar registro")
        print("5.-Buscar")
        print("6.-Salir")
        user_input = input("Introduce una opcion: ")
       
        #Insertar datos
        if user_input == "1":
            name = input("Ingrese su nombre: ")
            while name=="":
                name = input("Ingrese su nombre: ")
            
            last_name = input("Ingrese su apellido: ")
            while last_name=="":  
                last_name = input("Ingrese su apellido: ")
            
            identification = input("Ingrese su cedula: ")
            while identification=="" or identification.isdigit() != True:
                print("***No puede ingresar letras en la cedula***")
                identification = input("Ingrese su cedula: ")
            
            response = model.exists(identification)
            if(len(response) == 0):
                model.insert(name, last_name, identification)
                print("***Datos insertados correctamente***")
            else:
                print("La cedula ya existe")
            self.menu()
 
        #Consultar o ver todos los registros guardados
        elif user_input == "2":
            response = model.consultar()
            if response != []:
                for line in response:
                    name = line[1]
                    last_name = line[2]
                    identification = line[3]
                    print(f"Nombre: {name},  Apellido: {last_name}, cédula: {identification}")
            else:
                print("***La base de datos esta vacia***")
            self.menu()

        #Eliminar datos
        elif user_input == "3":
            identification = input("Ingrese la cedula para eliminar el registro: ")
            res = model.exists(identification)
            if len(res) > 0:
                response = model.delete(res[0][0])
                print("***Registro eliminado con exito***")
            else:
                print('***Cedula invalida***')
            self.menu()
        
        #Modificar o actualizar datos
        elif user_input == "4":
            print("***Bienvenido al menu de actualización de datos***")
            print("1.-Actualizar todo el registro")
            print("2.-Actualizar solo el nombre")
            print("3.-Actualizar solo el apellido")
            print("4.-Actualizar solo la cedula")
            option = input("Introduzca una opcion: ")

            if option == "1":
                identification = input("Ingrese la cedula para modificar el registro: ")
                res = model.exists(identification)
                if(len(res) > 0):
                    name = input("Ingrese el nombre: ")
                    last_name = input("Ingrese el apellido: ")
                    identification = input("Ingrese la cedula: ")
                    model.update(res[0][0], name, last_name, identification)
                else:
                    print("***Lo sentimos, el registro que busca actualizar no existe***")
                self.menu()

            if option == "2":
                identification = input("Ingrese la cedula para modificar el registro: ")
                res = model.exists(identification)
                if(len(res) > 0):
                    name = input("Ingrese el nuevo nombre: ")
                    last_name = res[0][2]
                    identification = res[0][3]
                    model.update(res[0][0], name,last_name,identification)
                else:
                    print("***Lo sentimos, el registro que busca actualizar no existe***")
                self.menu()

    
            if option == "3":
                identification = input("Ingrese la cedula para modificar el registro: ")
                res = model.exists(identification)
                if(len(res) > 0):
                    last_name = input("Ingrese nuevo apellido: ")
                    name = res[0][1]
                    identification = res[0][3]
                    model.update(res[0][0], name,last_name,identification)
                else:
                    print("***Lo sentimos, el registro que busca actualizar no existe***")
                self.menu()

            if option == "4":
                identification = input("Ingrese la cedula para modificar el registro: ")
                res = model.exists(identification)
                if(len(res) > 0):
                    identification = input("Ingrese la nueva cedula: ")
                    name = res[0][1]
                    last_name = res[0][2]
                    model.update(res[0][0], name,last_name,identification)
                else:
                    print("***Lo sentimos, el registro que busca actualizar no existe***")
                
                self.menu()

        #Buscar datos por cedula
        elif user_input == "5":
            identification = input("Ingrese la cedula que desea buscar: ")
            response = model.exists(identification)
            if response != []:
                for line in response:
                    name = line[1]
                    last_name = line[2]
                    identification = line[3]
                    print(f"Nombre: {name},  Apellido: {last_name}, cédula: {identification}")
            else:
                print("***Lo sentimos, el registro que busca no existe***")
            self.menu()
        
        #Cerrar el menu
        elif user_input == "6":
            print("***Cerrado con exito!!***")
            model.exit_user()
            

        else:
            print("***Introduzca una opción valida***")

controller = Controller()
controller.menu()
