import psycopg2
from Model import *

class Model():

     connection = psycopg2.connect(
     user="root", password="root", host="127.0.0.1", port="5436", database="db_students")
     cursor = connection.cursor()

     def create_Table(self):
        create = """CREATE TABLE IF NOT EXISTS student(id SERIAL PRIMARY KEY,name VARCHAR NOT NULL,last_name VARCHAR NOT NULL, identification VARCHAR NOT NULL);"""
        self.cursor.execute(create)
        self.connection.commit()
            
     def consultar(self):
        sql = "SELECT * FROM student"
        self.cursor.execute(sql)
        response = self.cursor.fetchall()
        return response

     def insert(self, name, last_name, identification):
        sql = "INSERT INTO student(name, last_name, identification) VALUES (%s,%s,%s)"
        datos = (name, last_name, identification)
        self.cursor.execute(sql, datos)
        self.connection.commit()

     def delete(self, id):
        sql = f'DELETE FROM student WHERE id = {id}'
        self.cursor.execute(sql)
        self.connection.commit()

     def update(self, id, name, last_name, identification):
        sql = f"UPDATE student set name='{name}', last_name='{last_name}', identification='{identification}' WHERE id={id}"
        self.cursor.execute(sql)
        self.connection.commit()

     def exists(self, identification):
        sql = f"SELECT * FROM student WHERE identification = '{identification}'"
        self.cursor.execute(sql)
        self.connection.commit()
        registroExists = self.cursor.fetchall()
        return registroExists
     
     def exit_user(self):
        self.cursor.close()
        self.connection.close()
        exit()
        
        

model = Model()
model.create_Table()
